package com.sgi.javaspringboot.userregistration.controller;

import com.sgi.javaspringboot.userregistration.model.AppUser;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.sgi.javaspringboot.userregistration.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.LocaleResolver;

@Controller
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/registration")
    public String showRegistrationForm(Model model) {
        AppUser user = new AppUser();
        user.setFirstName("Badu");
        user.setLastName("Budiman");
        user.setPassword("12345678");
        user.setEmail("badu@mail.com");
        model.addAttribute("user",user);

        return "registration";
    }

    @PostMapping("/registration") //@ModelAttribute
    public String registerUserAccount(@Valid AppUser user, Model model, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            System.out.println("ERROR DETECTED");
            model.addAttribute("user", user);
            return "registration";
        }
        System.out.println(user);
        AppUser _user = userRepository.save(user);
        /*System.out.println(_user);
        System.out.println(userRepository.findAll());*/
        return "redirect:/";
    }
}
