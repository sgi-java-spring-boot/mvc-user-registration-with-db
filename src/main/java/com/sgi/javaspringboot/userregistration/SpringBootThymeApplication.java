package com.sgi.javaspringboot.userregistration;

import org.springframework.boot.SpringApplication;

@org.springframework.boot.autoconfigure.SpringBootApplication
public class SpringBootThymeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootThymeApplication.class, args);
	}

}
