package com.sgi.javaspringboot.userregistration.service;

import com.sgi.javaspringboot.userregistration.model.AppUser;
import com.sgi.javaspringboot.userregistration.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<AppUser> user = userRepository.findByEmail(s);
        if (user.isPresent()) {
            AppUser appuser = user.get();
            return User.withUsername(appuser.getEmail()).password(appuser.getPassword()).authorities("USER").build();
        } else {
            throw new UsernameNotFoundException(String.format("Email[%s] not found", s));
        }
    }
}